import db from "../models";
exports.getUsers = async (req, res) => {
  try {
    let resUsers = await db.users.findAll({
      include: [
        {
          model: db.groups,
          as: "group_info",
        },
      ],
    });
    let obj = {
      users: resUsers,
      error: false,
    };
    return res.status(200).json(obj);
  } catch (error) {
    return res.status(500).json({ error:error.message });
  }
};
exports.createUser = async (req, res) => {
    let {username,password,group_id}=req.body
  try {
    let user = await db.users.create({
      username: username,
      password: password,
      group_id:group_id,
      createdAt: new Date(),
    });
    return res.status(200).json(user);
  } catch (error) {
    return res.status(500).json({ error });
  }
};
