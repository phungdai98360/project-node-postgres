module.exports = function (sequelize, DataTypes) {
    var groups = sequelize.define(
      "groups",
      {
        name: {
          type: DataTypes.STRING(255),
          // If a customer is to be created, they must have a name
          allowNull: false,
          //   unique: true
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: true,
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: true,
        },
      },
      {
        timestamps: false,
      }
    );
    groups.associate = function(models) {
    // associations can be defined here
    groups.hasMany(models.users, {
      foreignKey: 'group_id',
      as: 'list_user',
    //   onDelete: 'CASCADE',
    });
  };
    return groups;
}