// User model

// The User has a "username" attribute of type DataTypes.String
module.exports = function (sequelize, DataTypes) {
  var users = sequelize.define(
    "users",
    {
      username: {
        type: DataTypes.STRING(255),
        // If a customer is to be created, they must have a name
        allowNull: false,
        validate: {
          len: [6, 20],
        },
        //   unique: true
      },
      password: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
	  group_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      timestamps: false,
    }
  );
  users.associate = function(models) {
    // associations can be defined here
    users.belongsTo(models.groups, {
      foreignKey: 'group_id',
      as: 'group_info',
    });
  };
  return users;
};
