import express from 'express';
import userCtrl from '../controllers/users.ctrl'
const router = express.Router();
const requireLogin = require('../middleware/requireLogin')

// Get all users
router.get('/',userCtrl.getUsers)
// Create new user
router.post('/',userCtrl.createUser)

// Login 
// router.post('/login', (req, res) => {
// 	let options =   {
// 		where: {
// 		  	email: req.body.email
// 		  }
// 	};
// 	db.users.findOne(options).then(user => {
// 		if (!user) {
// 			res.json({ error: 'Invalid email or password.' });
// 		} 
// 		if (req.body.password === user.password) {
// 			// sets a cookie with the user's info
// 			//req.session.user = user;
// 			res.json({'message': 'successful login!'});
// 		} else {
// 			res.json({ error: 'Invalid email or password.' });
// 		}
// 	});
// });
module.exports = router;